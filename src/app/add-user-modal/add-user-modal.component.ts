import { DataService } from './../services/data.service';
import { User } from './../interfaces/user';
import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss']
})

export class AddUserModalComponent {
  @Output() closeEvent = new EventEmitter();
  public userName: string;
  public email: string;
  public age: number;

  constructor(private dataService: DataService) {}

  public closeModal(): void {
    this.closeEvent.next();
  }

  public saveUser(): void {
    if (this.userName && this.age && this.email) {
      const user: User = {
        id: Math.random().toString(),
        name: this.userName,
        email: this.email,
        age: this.age,
        avatar: 'assets/user.png'
      };

      for (const key in user) {
        console.log(key + '=' + user[key]);
      }


      this.dataService.newUserData = user;
      this.closeModal();
    }
  }
}
