import { logging } from 'protractor';
import { DataService } from './services/data.service';
import { Component, OnInit } from '@angular/core';
import { Result, User } from './interfaces/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent  implements OnInit {
  //public - переменая доступна в html
  //private - переменая доступна только в нутри ts файла
  //static - переменая на уровне класса

  public users: User[] = [];
  public isModalOpened: boolean;

  constructor(public dataService: DataService) {}

  ngOnInit(): void {
    console.log('App component inited');
    this.dataService.loadUserList().then(data => {
      this.users = data.results.map(item => this.convertToUser(item));
    });
  }

  public addUser(): void {
    this.isModalOpened = true;
  }

  public closeModal(): void {
    if (this.dataService.newUserData) {
      this.users.push(this.dataService.newUserData);
      this.dataService.newUserData = null;
    }
    this.isModalOpened = false;
  }

  public deleteUser(id: string): void {
    this.users = this.users.filter(item => item.id !== id);
  }

  private convertToUser(userData: Result): User {
    const {email, dob, picture} = userData;

    const id = userData.login.uuid;
    const age = dob.age;
    const name = userData.name.first + ' ' + userData.name.last;
    const avatar = picture.medium;

    const user: User = {
      id,
      age,
      email,
      name,
      avatar
    };
    return user;
  }
}
