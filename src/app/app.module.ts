import { DataService } from './services/data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MyBtnComponent } from './my-btn/my-btn.component';
import { CounterPipe } from './pipes/counter.pipe';
import { UserCardComponent } from './user-card/user-card.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddUserModalComponent } from './add-user-modal/add-user-modal.component';

@NgModule({
  declarations: [// компонеты, дерективы и пайпы
    AppComponent,
    MyBtnComponent,
    CounterPipe,
    UserCardComponent,
    AddUserModalComponent,
  ],
  imports: [ // модули
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [], // сервисы  << не нужен если есть 'root'
  bootstrap: [AppComponent] // стартовый компонент
})
export class AppModule { }
