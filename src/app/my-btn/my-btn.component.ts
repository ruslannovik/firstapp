import { Component } from "@angular/core";

@Component({ //это декоратор
    selector: 'app-my-btn',
    templateUrl: 'my-btn.component.html',
    styleUrls: ['my-btn.component.scss']
})

export class MyBtnComponent {
    public counter = 0;

    public addCount() {
        this.counter++;
    }
}

