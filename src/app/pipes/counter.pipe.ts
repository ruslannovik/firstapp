import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'appCounterPipe'
})

export class CounterPipe implements PipeTransform {
    transform(value: number) {
        return value < 10 ? 'Мало' : 'Много';
    }
}