import { BackendUsersData, User } from './../interfaces/user';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({providedIn: 'root'})

export class DataService {

  public newUserData: User;

  constructor(private http: HttpClient) {}

  public loadUserList(): Promise<BackendUsersData> {
    return this.http.get<BackendUsersData>('https://randomuser.me/api/?results=10')
      .toPromise();
  }
}
