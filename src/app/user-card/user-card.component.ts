import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../interfaces/user';

@Component({
    selector: 'app-user-card',
    templateUrl: 'user-card.component.html',
    styleUrls: ['user-card.component.scss']
})

export class UserCardComponent {
    static editModeCardId: string;

    @Input() public user: User;
    @Output() public userDelete = new EventEmitter();

    public onDelete(): void {
        this.userDelete.next(this.user.id);
    }

    public toggleEditMode(): void {
      if (UserCardComponent.editModeCardId === this.user.id) {
        UserCardComponent.editModeCardId = null;
      } else {
        UserCardComponent.editModeCardId = this.user.id;
      }
    }

    public get isEditState(): boolean {
      return UserCardComponent.editModeCardId === this.user.id;
    }
}
